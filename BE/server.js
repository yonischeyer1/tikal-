const express = require('express')
const app = express()
const port = 3001
const axios = require('axios')
const cors = require('cors')
const bodyParser = require('body-parser')
app.use(cors({
    exposedHeaders: {
        "port": 3001,
        "bodyLimit": "100kb",
        "corsHeaders": ["Link"]
    }
}));
app.use(bodyParser.json({
    limit: "100kb"
}));
app.post('/search', (req, res) => {
    const data = req.body;
    const requestURL = `http://localhost:9200/mi6/agents/_search/?size=1000`;
    axios.get(requestURL, { data }).then((result) => {
        res.send(result.data)
    });
})
app.get('/countries-by-isolation', (req, res) => {
    const data = req.body;
    const requestURL = `http://localhost:9200/mi6/agents/_search`;
    axios.get(requestURL, { data }).then((result) => {
        res.send(result.data)
    });
})
app.post('/find-closest', (req, res) => {
    const data = req.body;
    const requestURL = `http://localhost:9200/auto/cars/_search`;
    axios.get(requestURL, { data }).then((result) => {
        res.send(result.data)
    });
})
app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
    setTimeout(() => { populateDB() })
}, 1000)
function populateDB() {
    const agents = [
        {
            agent: '007', country: 'Brazil',
            address: 'Avenida Vieira Souto 168 Ipanema, Rio de Janeiro',
            date: 'Dec 17, 1995, 9:45:17 PM'
        },
        {
            agent: '005', country: 'Poland',
            address: 'Rynek Glowny 12, Krakow',
            date: 'Apr 5, 2011, 5:05:12 PM'
        },
        {
            agent: '007', country: 'Morocco',
            address: '27 Derb Lferrane, Marrakech',
            date: 'Jan 1, 2001, 12:00:00 AM'
        },
        {
            agent: '005', country: 'Brazil',
            address: 'Rua Roberto Simonsen 122, Sao Paulo',
            date: 'May 5, 1986, 8:40:23 AM'
        },
        {
            agent: '011', country: 'Poland',
            address: 'swietego Tomasza 35, Krakow',
            date: 'Sep 7, 1997, 7:12:53 PM'
        },
        {
            agent: '003', country: 'Morocco',
            address: 'Rue Al-Aidi Ali Al-Maaroufi, Casablanca',
            date: 'Aug 29, 2012, 10:17:05 AM'
        },
        {
            agent: '008', country: 'Brazil',
            address: 'Rua tamoana 418, tefe',
            date: 'Nov 10, 2005, 1:25:13 PM'
        },
        {
            agent: '013', country: 'Poland',
            address: 'Zlota 9, Lublin',
            date: 'Oct 17, 2002, 10:52:19 AM'
        },
        {
            agent: '002', country: 'Morocco',
            address: 'Riad Sultan 19, Tangier',
            date: 'Jan 1, 2017, 5:00:00 PM'
        },
        {
            agent: '009', country: 'Morocco',
            address: 'atlas marina beach, agadir',
            date: 'Dec 1, 2016, 9:21:21 PM'
        }
    ]
    for (let i = 1; i < agents.length; i++) {
        const requestURL = `http://localhost:9200/mi6/agents/${i}/_update`;
        console.log(axios.post(requestURL, { doc: { agent:"005", "doc_as_upsert": true } }));
    }
}