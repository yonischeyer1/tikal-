import React, { Component } from 'react';
import { connect } from 'react-redux'
import './MainView.style.scss';
import { getSearchResults } from '../../redux/actions/search.actions'
import Api from '../../redux/api'
class Search extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.dispatch(getSearchResults())
    }
    render() {
        return (
            <div className="search-container">
                <br />
                <table className="agent-table">
                    <thead>
                        <tr>
                            <th>Agent ID</th>
                            <th>Country</th>
                            <th>Address</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                          this.props.searchResults.map((result, index) => {
                              return <tr key={index}>
                                  <td>{result._source.agent}</td>
                                  <td>{result._source.country}</td>
                                  <td>{result._source.address}</td>
                                  <td>{result._source.date}</td>
                              </tr>
                          })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    searchResults: [...state.searchResults]
});



export default connect(mapStateToProps)(Search);
