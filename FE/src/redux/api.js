import axios from 'axios';

const api = {
    insert_data: (data, index, doc_type) => {
        const requestURL = `http://localhost:9200/${index}/${doc_type}`;
        for(let agent of data) {
            axios.post(requestURL, agent); 
        }
        return "done"
    },
    search: (index, doc_type, keyword) => {
        const requestURL = `http://localhost:3001/search`;
        return axios.post(requestURL, {
            "query": {
                "match_all": {
                }
            }
        });
    }
};

export default api;