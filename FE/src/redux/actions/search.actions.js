export const getSearchResults = (payload) => ({
    type: 'GET_SEARCH_RESULTS',
    payload
});

export const updateSearchResults = (payload) => ({
    type: 'UPDATE_SEARCH_RESULTS',
    payload
});


export const keywordSearchFaild = (payload) =>({
    type: 'KEYWORD_SEARCH_FAILD',
    payload  
})